import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-draw-details',
  templateUrl: './draw-details.component.html',
  styleUrls: ['./draw-details.component.css']
})
export class DrawDetailsComponent implements OnInit {

  public arLang = false;

  constructor(
    private translate: TranslateService
  ) {
    this.arLang = (translate.currentLang === 'ar');
}

ngOnInit() {
  this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
    this.arLang = (event.lang === 'ar');
  });
}

}
