import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawWinnersComponent } from './draw-winners.component';

describe('DrawWinnersComponent', () => {
  let component: DrawWinnersComponent;
  let fixture: ComponentFixture<DrawWinnersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawWinnersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawWinnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
