import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-draw-winners',
  templateUrl: './draw-winners.component.html',
  styleUrls: ['./draw-winners.component.css']
})
export class DrawWinnersComponent implements OnInit {

  public arLang = false;
  public winners = false;

  constructor(
    private translate: TranslateService
  ) {
    this.arLang = (translate.currentLang === 'ar');
}


  public firstWinnersList = [
    {
      country: 'Kingdom of Saudi Arabia',
      countryAR: 'المملكة العربية السعودية',
      winners: ['Alice Smith', 'Claire Martin', 'Lucy Michel']
    },
    {
      country: 'United Arab Emirates',
      countryAR: 'الإمارات العربية المتحدة',
      winners: ['Annie Gracia', 'Patricia Bertrand']
    },
    {
      country: 'Kuwait',
      countryAR: 'الكويت',
      winners: ['Pamela Andrea', 'Sohpie Fournier']
    },
    // {
    //   country: 'Lebanon',
    //   winners: ['Charlotte Mercier']
    // }
  ];

  public secondWinnersList = [
    {
      country: 'Kingdom of Saudi Arabia',
      countryAR: 'المملكة العربية السعودية',
      winners: ['Alice Smith', 'Claire Martin', 'Lucy Michel']
    },
    {
      country: 'United Arab Emirates',
      countryAR: 'الإمارات العربية المتحدة',
      winners: ['Annie Gracia', 'Patricia Bertrand']
    },
    {
      country: 'Kuwait',
      countryAR: 'الكويت',
      winners: ['Pamela Andrea', 'Sohpie Fournier']
    },
    // {
    //   country: 'Lebanon',
    //   winners: ['Charlotte Mercier']
    // }
  ];

  ngOnInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.arLang = (event.lang === 'ar');
    });
  }

  drawWinners() {
    this.winners = !this.winners;
  }

}
