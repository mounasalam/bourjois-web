import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.css']
})
export class TermsAndConditionsComponent implements OnInit {

  public arLang = false;

  constructor(
    private translate: TranslateService
  ) {
    this.arLang = (translate.currentLang === 'ar');
}

ngOnInit() {
  window.scroll(0, 0);
  this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
    this.arLang = (event.lang === 'ar');
  });
}

}
