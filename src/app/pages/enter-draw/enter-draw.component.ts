import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enter-draw',
  templateUrl: './enter-draw.component.html',
  styleUrls: ['./enter-draw.component.css']
})
export class EnterDrawComponent implements OnInit {

  public arLang = false;
  public loading = false;
  public disabled = false;
  public form: FormGroup;
  public codeInvalid = false;
  public codePattern: RegExp = /^BRJ-[1-9]{4}/;
  public emailPattern: RegExp =
  // tslint:disable-next-line: max-line-length
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  public Code = new FormControl('', [Validators.required]);
  public Invoice = new FormControl('', Validators.required);
  public Email = new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]);

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private http: HttpService,
    private translate: TranslateService
    ) {
      this.form = fb.group({
        code: this.Code,
        email: this.Email,
        invoice: this.Invoice,
      });
      this.arLang = (translate.currentLang === 'ar');
    }

  ngOnInit() {

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.arLang = (event.lang === 'ar');
    });
  }

  enterDraw() {
    if (this.form.valid) {
      this.disabled = true;
      this.loading = true;
      const code = this.form.value.code;
      const invoice = this.form.value.invoice;
      const email = this.form.value.email;
      this.http.addTokenstoCandidates(email, code, invoice).then((res) => {
        this.loading = false;
        this.router.navigate(['congratulations']);
      }, (err) => {
        if (err.error.Message === 'The provided email does not exist.') {
          localStorage.setItem('code', code);
          localStorage.setItem('email', email);
          localStorage.setItem('invoice', invoice);
          this.router.navigate(['registration']);
        } else {
          this.codeInvalid = true;
          setTimeout(() => {
            this.codeInvalid = false;
          }, 4000);
          this.loading = false;
          this.disabled = false;
        }
      });
    }
  }

}
