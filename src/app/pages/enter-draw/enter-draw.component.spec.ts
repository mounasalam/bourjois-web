import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterDrawComponent } from './enter-draw.component';

describe('EnterDrawComponent', () => {
  let component: EnterDrawComponent;
  let fixture: ComponentFixture<EnterDrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterDrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterDrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
