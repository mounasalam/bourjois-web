import { COUNTRIES, COUNTRIESAR, NATIONALITIES, NATIONALITIESAR } from 'src/app/shared/countries/countries';
import { Component, HostListener, OnInit } from '@angular/core';
import { DAYS, MONTHS, MONTHSAR, YEARS } from 'src/app/shared/dates/dates';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

import { Candidates } from 'src/app/shared/candidates/candidates';
import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  public emailPattern: RegExp =
    // tslint:disable-next-line: max-line-length
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public codePattern: RegExp = /^BRJ-[1-9]{4}/;

  public form: FormGroup;

  public codeExist = false;
  public Terms = new FormControl('');
  public Phone = new FormControl('', Validators.required);
  public Invoice = new FormControl(null, Validators.required);
  public Month = new FormControl(null, Validators.required);
  public Day = new FormControl(null, Validators.required);
  public Store = new FormControl(null, Validators.required);
  public FullName = new FormControl('', Validators.required);
  public Gender = new FormControl(null, Validators.required);
  public Country = new FormControl(null, Validators.required);
  public Nationality = new FormControl(null, Validators.required);
  public DateofBirth = new FormControl(null, Validators.required);
  public Email = new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]);
  public Code = new FormControl('', [Validators.required, Validators.maxLength(8), Validators.minLength(8)]);

  public days = DAYS;
  public years = YEARS;
  public agree = false;
  public submit = false;
  public arLang = false;
  public months = MONTHS;
  public countries = COUNTRIES;
  public nationalities = NATIONALITIES;
  public genderAr = [
    { name: 'أنثى' },
    { name: 'ذكر' },
  ];
  public genderEn = [
    { name: 'Female' },
    { name: 'Male' },
  ];
  public gender: any;
  public telOptions =
      { initialCountry: 'kw',
      onlyCountries: ['kw', 'ae', 'sa']};

  public loading = false;
  public disabled = false;
  public phoneNumber: string;
  public phoneError = false;
  public codeInvalid = false;
  public phoneInvalid = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private http: HttpService,
    public translate: TranslateService
  ) {
    this.form = fb.group({
      day: this.Day,
      code: this.Code,
      store: this.Store,
      phone: this.Phone,
      email: this.Email,
      terms: this.Terms,
      month: this.Month,
      gender: this.Gender,
      name: this.FullName,
      country: this.Country,
      invoice: this.Invoice,
      year: this.DateofBirth,
      nationality: this.Nationality,
    });
    this.arLang = (translate.currentLang === 'ar');
    if (this.arLang) {
      this.months = MONTHSAR;
      this.gender = this.genderAr;
      this.countries = COUNTRIESAR;
      this.nationalities = NATIONALITIESAR;
    } else {
      this.gender = this.genderEn;
      this.months = MONTHS;
      this.countries = COUNTRIES;

      this.nationalities = NATIONALITIES;
    }
  }

  ngOnInit() {
    const code = localStorage.getItem('code');
    const email = localStorage.getItem('email');
    const invoice = localStorage.getItem('invoice');
    if (code != null) {
      this.codeExist = true;
      this.form.controls.code.setValue(code);
    }
    if (email != null) {
      this.form.controls.email.setValue(email);
    }
    if (invoice != null) {
      this.form.controls.invoice.setValue(invoice);
    }
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.arLang = (event.lang === 'ar');
      if (this.arLang) {
        this.nationalities = NATIONALITIESAR;
        this.months = MONTHSAR;
        this.gender = this.genderAr;
        this.countries = COUNTRIESAR;
      } else {
        this.months = MONTHS;
        this.gender = this.genderEn;
        this.countries = COUNTRIES;
        this.nationalities = NATIONALITIES;
      }
    });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    localStorage.setItem('candidate', JSON.stringify(this.form.value));
  }

  agreeTerms(event) {
    this.agree = event.target.checked;
  }

  register() {
    this.submit = true;
    if (!this.phoneError) {
      // phone
      return;
    }
    this.loading = true;
    if (this.form.valid && this.agree) {
      this.disabled = true;
      const birth = `${this.form.value.month} ${this.form.value.day}, ${this.form.value.year}`;
      const candidate: Candidates = {
        DateOfBirth: birth,
        Token: this.form.value.code,
        Email: this.form.value.email,
        Gender: this.form.value.gender,
        FullName: this.form.value.name,
        Country: this.form.value.country,
        PhoneNumber: this.phoneNumber,
        Nationality: this.form.value.nationality,
        RetailerInvoiceNumber: this.form.value.invoice,
      };
      this.http.addCandidate(candidate).then((res) => {
        this.loading = false;
        localStorage.clear();
        this.form.reset();
        this.router.navigate(['congratulations']);
      }, (err) => {
        if (err.error.Message === 'Provided token does not exist or was previously used.') {
          this.codeInvalid = true;
          setTimeout(() => {
            this.codeInvalid = false;
          }, 4000);
          this.loading = false;
          this.disabled = false;
        } else {
        }
      });
    } else {
      setTimeout(() => {
        this.loading = false;
      }, 1000);
    }
  }

  telInputObject(event) {
  }

  hasError(event) {
    this.phoneError = event;
    this.phoneInvalid = event;
  }

  onCountryChange(event) {
  }

  getNumber(event) {
    this.phoneNumber = event;
  }

  onCountrySelect(event) {
    this.telOptions.initialCountry = event.codeTwoDigit;
  }
}
