import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-congratulations',
  templateUrl: './congratulations.component.html',
  styleUrls: ['./congratulations.component.css']
})
export class CongratulationsComponent implements OnInit {

  public arLang = false;

  constructor(
    private translate: TranslateService
  ) {
    this.arLang = (translate.currentLang === 'ar');
}

ngOnInit() {
  this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
    this.arLang = (event.lang === 'ar');
  });
}
}
