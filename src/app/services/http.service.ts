// import { Http, RequestOptions, Headers } from "@angular/common/http";
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Candidates } from '../shared/candidates/candidates';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  public url = 'https://bourjois-release.azurewebsites.net';

  constructor(
    private httpClient: HttpClient
  ) { }

  getTokens(): any {
    const url = `${this.url}/Api/Tokens`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    return this.httpClient.get(url, { headers })
      .pipe(map(res => {
        return res;
      })).toPromise();
  }

  addCandidate(candidate: Candidates): any {
    const url = `${this.url}/Api/Candidates`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    return this.httpClient.post(url, candidate, { headers })
    .pipe(map(res => {
      return res;
    })).toPromise();
  }

  addTokenstoCandidates(email: string, token: string, invoice: string): any {
    const url = `${this.url}/Api/Candidates/${email}/tokens/${token}/invoice/${invoice}`;
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    return this.httpClient.post(url, {}, { headers })
    .pipe(map(res => {
      return res;
    })).toPromise();
  }

}
