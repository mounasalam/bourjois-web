import 'please-wait/build/please-wait.css';

import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { pleaseWait } from 'please-wait';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'bourjois-web';
  public loadingScreen: any;
  constructor( private translate: TranslateService ) {

      translate.addLangs(['en', 'ar']);
      // translate.setDefaultLang('en');

      const browserLang = localStorage.getItem('lan');
      if (browserLang) {
        translate.use(browserLang.match(/en|ar/) ? browserLang : 'en');
      } else {
        translate.use('en');
      }
      this.showLoading();
      setTimeout(() => {
        this.hideLoading();
      }, 6000);
  }
  onScroll(event: Event) {

  }

  showLoading() {

    this.loadingScreen = pleaseWait({
     logo: 'assets/imgs/logo-bourjois-colored.png',
     backgroundColor: 'white',
     loadingHtml: `<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>`
   });
  }

  hideLoading() {
    if (this.loadingScreen) {
      this.loadingScreen.finish();
    }
  }

}
