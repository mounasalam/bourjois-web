import { RouterModule, Routes } from '@angular/router';

import { CongratulationsComponent } from './pages/congratulations/congratulations.component';
import { DrawDetailsComponent } from './pages/draw-details/draw-details.component';
import { DrawWinnersComponent } from './pages/draw-winners/draw-winners.component';
import { EnterDrawComponent } from './pages/enter-draw/enter-draw.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { TermsAndConditionsComponent } from './pages/terms-and-conditions/terms-and-conditions.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'enter-draw',
    component: EnterDrawComponent
  },
  {
    path: 'draw-details',
    component: DrawDetailsComponent
  },
  {
    path: 'draw-winners',
    component: DrawWinnersComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'congratulations',
    component: CongratulationsComponent
  },
  {
    path: 'terms-conditions',
    component: TermsAndConditionsComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: '**',
    component: HomeComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { useHash: true, scrollPositionRestoration: 'enabled'}
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
