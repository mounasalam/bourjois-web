export class Dates {
  year?: string;
  month?: string;
  abbr?: string;
  day?: string;
}

export const YEARS: Dates[] = [
  { year: '2010' },
  { year: '2009' },
  { year: '2009' },
  { year: '2007' },
  { year: '2006' },
  { year: '2005' },
  { year: '2004' },
  { year: '2003' },
  { year: '2002' },
  { year: '2001' },
  { year: '2000' },
  { year: '1999' },
  { year: '1998' },
  { year: '1997' },
  { year: '1996' },
  { year: '1995' },
  { year: '1994' },
  { year: '1993' },
  { year: '1992' },
  { year: '1991' },
  { year: '1990' },
  { year: '1989' },
  { year: '1988' },
  { year: '1987' },
  { year: '1986' },
  { year: '1985' },
  { year: '1984' },
  { year: '1983' },
  { year: '1982' },
  { year: '1981' },
  { year: '1980' },
  { year: '1979' },
  { year: '1978' },
  { year: '1977' },
  { year: '1976' },
  { year: '1975' },
  { year: '1974' },
  { year: '1973' },
  { year: '1972' },
  { year: '1971' },
  { year: '1970' },
  { year: '1969' },
  { year: '1968' },
  { year: '1967' },
  { year: '1966' },
  { year: '1965' },
  { year: '1964' },
  { year: '1963' },
  { year: '1962' },
  { year: '1961' },
  { year: '1960' },
  { year: '1959' },
  { year: '1958' },
  { year: '1957' },
  { year: '1956' },
  { year: '1955' },
  { year: '1954' },
  { year: '1953' },
  { year: '1952' },
  { year: '1951' },
  { year: '1950' },
  { year: '1949' },
  { year: '1948' },
  { year: '1947' },
  { year: '1946' },
  { year: '1945' },
  { year: '1944' },
  { year: '1943' },
  { year: '1942' },
  { year: '1941' },
  { year: '1940' },
];

export const MONTHS: Dates[] = [
    {
      abbr: 'Jan',
      month: 'January'
    },
    {
      abbr: 'Feb',
      month: 'February'
    },
    {
      abbr: 'Mar',
      month: 'March'
    },
    {
      abbr: 'Apr',
      month: 'April'
    },
    {
      abbr: 'May',
      month: 'May'
    },
    {
      abbr: 'Jun',
      month: 'June'
    },
    {
      abbr: 'Jul',
      month: 'July'
    },
    {
      abbr: 'Aug',
      month: 'August'
    },
    {
      abbr: 'Sep',
      month: 'September'
    },
    {
      abbr: 'Oct',
      month: 'October'
    },
    {
      abbr: 'Nov',
      month: 'November'
    },
    {
      abbr: 'Dec',
      month: 'December'
    }
];
export const MONTHSAR: Dates[] = [
    {
      abbr: 'Jan',
      month: 'يناير'
    },
    {
      abbr: 'Feb',
      month: 'فبراير'
    },
    {
      abbr: 'Mar',
      month: 'مارس'
    },
    {
      abbr: 'Apr',
      month: 'أبريل'
    },
    {
      abbr: 'May',
      month: 'مايو'
    },
    {
      abbr: 'Jun',
      month: 'يونيو'
    },
    {
      abbr: 'Jul',
      month: 'يوليو'
    },
    {
      abbr: 'Aug',
      month: 'أغسطس'
    },
    {
      abbr: 'Sep',
      month: 'سبتمبر'
    },
    {
      abbr: 'Oct',
      month: 'أكتوبر'
    },
    {
      abbr: 'Nov',
      month: 'نوفمبر'
    },
    {
      abbr: 'Dec',
      month: 'ديسمبر'
    }
];

export const DAYS: Dates[] = [
    {
      day: '1'
    }, {
      day: '2'
    }, {
      day: '3'
    }, {
      day: '4'
    }, {
      day: '5'
    }, {
      day: '6'
    }, {
      day: '7'
    }, {
      day: '8'
    }, {
      day: '9'
    }, {
      day: '10'
    }, {
      day: '11'
    }, {
      day: '12'
    }, {
      day: '13'
    }, {
      day: '14'
    }, {
      day: '15'
    }, {
      day: '16'
    }, {
      day: '17'
    }, {
      day: '18'
    }, {
      day: '19'
    }, {
      day: '20'
    }, {
      day: '21'
    }, {
      day: '22'
    }, {
      day: '23'
    }, {
      day: '24'
    }, {
      day: '25'
    }, {
      day: '26'
    }, {
      day: '27'
    }, {
      day: '28'
    }, {
      day: '29'
    }, {
      day: '30'
    }, {
      day: '31'
    }
  ];
