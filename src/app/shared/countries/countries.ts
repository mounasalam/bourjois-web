export class Countries {
  public name?: string;
  public numCode?: string;
  public dialCode?: string;
  public nationality?: string;
  public codeTwoDigit?: string;
  public codeThreeDigit?: string;
}

export const NATIONALITIES: Countries[] = [{
  numCode: '4',
  codeTwoDigit: 'AF',
  codeThreeDigit: 'AFG',
  name: 'Afghanistan',
  nationality: 'Afghan'
},
{
  numCode: '248',
  codeTwoDigit: 'AX',
  codeThreeDigit: 'ALA',
  name: 'Aland Islands',
  nationality: 'Aland Island'
},
{
  numCode: '8',
  codeTwoDigit: 'AL',
  codeThreeDigit: 'ALB',
  name: 'Albania',
  nationality: 'Albanian'
},
{
  numCode: '12',
  codeTwoDigit: 'DZ',
  codeThreeDigit: 'DZA',
  name: 'Algeria',
  nationality: 'Algerian'
},
{
  numCode: '16',
  codeTwoDigit: 'AS',
  codeThreeDigit: 'ASM',
  name: 'American Samoa',
  nationality: 'American Samoan'
},
{
  numCode: '20',
  codeTwoDigit: 'AD',
  codeThreeDigit: 'AND',
  name: 'Andorra',
  nationality: 'Andorran'
},
{
  numCode: '24',
  codeTwoDigit: 'AO',
  codeThreeDigit: 'AGO',
  name: 'Angola',
  nationality: 'Angolan'
},
{
  numCode: '660',
  codeTwoDigit: 'AI',
  codeThreeDigit: 'AIA',
  name: 'Anguilla',
  nationality: 'Anguillan'
},
{
  numCode: '10',
  codeTwoDigit: 'AQ',
  codeThreeDigit: 'ATA',
  name: 'Antarctica',
  nationality: 'Antarctic'
},
{
  numCode: '28',
  codeTwoDigit: 'AG',
  codeThreeDigit: 'ATG',
  name: 'Antigua and Barbuda',
  nationality: 'Antiguan or Barbudan'
},
{
  numCode: '32',
  codeTwoDigit: 'AR',
  codeThreeDigit: 'ARG',
  name: 'Argentina',
  nationality: 'Argentine'
},
{
  numCode: '51',
  codeTwoDigit: 'AM',
  codeThreeDigit: 'ARM',
  name: 'Armenia',
  nationality: 'Armenian'
},
{
  numCode: '533',
  codeTwoDigit: 'AW',
  codeThreeDigit: 'ABW',
  name: 'Aruba',
  nationality: 'Aruban'
},
{
  numCode: '36',
  codeTwoDigit: 'AU',
  codeThreeDigit: 'AUS',
  name: 'Australia',
  nationality: 'Australian'
},
{
  numCode: '40',
  codeTwoDigit: 'AT',
  codeThreeDigit: 'AUT',
  name: 'Austria',
  nationality: 'Austrian'
},
{
  numCode: '31',
  codeTwoDigit: 'AZ',
  codeThreeDigit: 'AZE',
  name: 'Azerbaijan',
  nationality: 'Azerbaijani, Azeri'
},
{
  numCode: '44',
  codeTwoDigit: 'BS',
  codeThreeDigit: 'BHS',
  name: 'Bahamas',
  nationality: 'Bahamian'
},
{
  numCode: '48',
  codeTwoDigit: 'BH',
  codeThreeDigit: 'BHR',
  name: 'Bahrain',
  nationality: 'Bahraini'
},
{
  numCode: '50',
  codeTwoDigit: 'BD',
  codeThreeDigit: 'BGD',
  name: 'Bangladesh',
  nationality: 'Bangladeshi'
},
{
  numCode: '52',
  codeTwoDigit: 'BB',
  codeThreeDigit: 'BRB',
  name: 'Barbados',
  nationality: 'Barbadian'
},
{
  numCode: '112',
  codeTwoDigit: 'BY',
  codeThreeDigit: 'BLR',
  name: 'Belarus',
  nationality: 'Belarusian'
},
{
  numCode: '56',
  codeTwoDigit: 'BE',
  codeThreeDigit: 'BEL',
  name: 'Belgium',
  nationality: 'Belgian'
},
{
  numCode: '84',
  codeTwoDigit: 'BZ',
  codeThreeDigit: 'BLZ',
  name: 'Belize',
  nationality: 'Belizean'
},
{
  numCode: '204',
  codeTwoDigit: 'BJ',
  codeThreeDigit: 'BEN',
  name: 'Benin',
  nationality: 'Beninese, Beninois'
},
{
  numCode: '60',
  codeTwoDigit: 'BM',
  codeThreeDigit: 'BMU',
  name: 'Bermuda',
  nationality: 'Bermudian, Bermudan'
},
{
  numCode: '64',
  codeTwoDigit: 'BT',
  codeThreeDigit: 'BTN',
  name: 'Bhutan',
  nationality: 'Bhutanese'
},
{
  numCode: '68',
  codeTwoDigit: 'BO',
  codeThreeDigit: 'BOL',
  name: 'Bolivia (Plurinational State of)',
  nationality: 'Bolivian'
},
{
  numCode: '535',
  codeTwoDigit: 'BQ',
  codeThreeDigit: 'BES',
  name: 'Bonaire, Sint Eustatius and Saba',
  nationality: 'Bonaire'
},
{
  numCode: '70',
  codeTwoDigit: 'BA',
  codeThreeDigit: 'BIH',
  name: 'Bosnia and Herzegovina',
  nationality: 'Bosnian or Herzegovinian'
},
{
  numCode: '72',
  codeTwoDigit: 'BW',
  codeThreeDigit: 'BWA',
  name: 'Botswana',
  nationality: 'Motswana, Botswanan'
},
{
  numCode: '74',
  codeTwoDigit: 'BV',
  codeThreeDigit: 'BVT',
  name: 'Bouvet Island',
  nationality: 'Bouvet Island'
},
{
  numCode: '76',
  codeTwoDigit: 'BR',
  codeThreeDigit: 'BRA',
  name: 'Brazil',
  nationality: 'Brazilian'
},
{
  numCode: '86',
  codeTwoDigit: 'IO',
  codeThreeDigit: 'IOT',
  name: 'British Indian Ocean Territory',
  nationality: 'BIOT'
},
{
  numCode: '96',
  codeTwoDigit: 'BN',
  codeThreeDigit: 'BRN',
  name: 'Brunei Darussalam',
  nationality: 'Bruneian'
},
{
  numCode: '100',
  codeTwoDigit: 'BG',
  codeThreeDigit: 'BGR',
  name: 'Bulgaria',
  nationality: 'Bulgarian'
},
{
  numCode: '854',
  codeTwoDigit: 'BF',
  codeThreeDigit: 'BFA',
  name: 'Burkina Faso',
  nationality: 'Burkinabe'
},
{
  numCode: '108',
  codeTwoDigit: 'BI',
  codeThreeDigit: 'BDI',
  name: 'Burundi',
  nationality: 'Burundian'
},
{
  numCode: '132',
  codeTwoDigit: 'CV',
  codeThreeDigit: 'CPV',
  name: 'Cabo Verde',
  nationality: 'Cabo Verdean'
},
{
  numCode: '116',
  codeTwoDigit: 'KH',
  codeThreeDigit: 'KHM',
  name: 'Cambodia',
  nationality: 'Cambodian'
},
{
  numCode: '120',
  codeTwoDigit: 'CM',
  codeThreeDigit: 'CMR',
  name: 'Cameroon',
  nationality: 'Cameroonian'
},
{
  numCode: '124',
  codeTwoDigit: 'CA',
  codeThreeDigit: 'CAN',
  name: 'Canada',
  nationality: 'Canadian'
},
{
  numCode: '136',
  codeTwoDigit: 'KY',
  codeThreeDigit: 'CYM',
  name: 'Cayman Islands',
  nationality: 'Caymanian'
},
{
  numCode: '140',
  codeTwoDigit: 'CF',
  codeThreeDigit: 'CAF',
  name: 'Central African Republic',
  nationality: 'Central African'
},
{
  numCode: '148',
  codeTwoDigit: 'TD',
  codeThreeDigit: 'TCD',
  name: 'Chad',
  nationality: 'Chadian'
},
{
  numCode: '152',
  codeTwoDigit: 'CL',
  codeThreeDigit: 'CHL',
  name: 'Chile',
  nationality: 'Chilean'
},
{
  numCode: '156',
  codeTwoDigit: 'CN',
  codeThreeDigit: 'CHN',
  name: 'China',
  nationality: 'Chinese'
},
{
  numCode: '162',
  codeTwoDigit: 'CX',
  codeThreeDigit: 'CXR',
  name: 'Christmas Island',
  nationality: 'Christmas Island'
},
{
  numCode: '166',
  codeTwoDigit: 'CC',
  codeThreeDigit: 'CCK',
  name: 'Cocos (Keeling) Islands',
  nationality: 'Cocos Island'
},
{
  numCode: '170',
  codeTwoDigit: 'CO',
  codeThreeDigit: 'COL',
  name: 'Colombia',
  nationality: 'Colombian'
},
{
  numCode: '174',
  codeTwoDigit: 'KM',
  codeThreeDigit: 'COM',
  name: 'Comoros',
  nationality: 'Comoran, Comorian'
},
{
  numCode: '180',
  codeTwoDigit: 'CD',
  codeThreeDigit: 'COD',
  name: 'Congo (Democratic Republic of the)',
  nationality: 'Congolese'
},
{
  numCode: '184',
  codeTwoDigit: 'CK',
  codeThreeDigit: 'COK',
  name: 'Cook Islands',
  nationality: 'Cook Island'
},
{
  numCode: '188',
  codeTwoDigit: 'CR',
  codeThreeDigit: 'CRI',
  name: 'Costa Rica',
  nationality: 'Costa Rican'
},
{
  numCode: '384',
  codeTwoDigit: 'CI',
  codeThreeDigit: 'CIV',
  name: 'C\u00f4te d\'Ivoire',
  nationality: 'Ivorian'
},
{
  numCode: '191',
  codeTwoDigit: 'HR',
  codeThreeDigit: 'HRV',
  name: 'Croatia',
  nationality: 'Croatian'
},
{
  numCode: '192',
  codeTwoDigit: 'CU',
  codeThreeDigit: 'CUB',
  name: 'Cuba',
  nationality: 'Cuban'
},
{
  numCode: '531',
  codeTwoDigit: 'CW',
  codeThreeDigit: 'CUW',
  name: 'Cura\u00e7ao',
  nationality: 'Cura\u00e7aoan'
},
{
  numCode: '196',
  codeTwoDigit: 'CY',
  codeThreeDigit: 'CYP',
  name: 'Cyprus',
  nationality: 'Cypriot'
},
{
  numCode: '203',
  codeTwoDigit: 'CZ',
  codeThreeDigit: 'CZE',
  name: 'Czech Republic',
  nationality: 'Czech'
},
{
  numCode: '208',
  codeTwoDigit: 'DK',
  codeThreeDigit: 'DNK',
  name: 'Denmark',
  nationality: 'Danish'
},
{
  numCode: '262',
  codeTwoDigit: 'DJ',
  codeThreeDigit: 'DJI',
  name: 'Djibouti',
  nationality: 'Djiboutian'
},
{
  numCode: '214',
  codeTwoDigit: 'DO',
  codeThreeDigit: 'DOM',
  name: 'Dominican Republic',
  nationality: 'Dominican'
},
{
  numCode: '218',
  codeTwoDigit: 'EC',
  codeThreeDigit: 'ECU',
  name: 'Ecuador',
  nationality: 'Ecuadorian'
},
{
  numCode: '818',
  codeTwoDigit: 'EG',
  codeThreeDigit: 'EGY',
  name: 'Egypt',
  nationality: 'Egyptian'
},
{
  numCode: '222',
  codeTwoDigit: 'SV',
  codeThreeDigit: 'SLV',
  name: 'El Salvador',
  nationality: 'Salvadoran'
},
{
  numCode: '226',
  codeTwoDigit: 'GQ',
  codeThreeDigit: 'GNQ',
  name: 'Equatorial Guinea',
  nationality: 'Equatorial Guinean, Equatoguinean'
},
{
  numCode: '232',
  codeTwoDigit: 'ER',
  codeThreeDigit: 'ERI',
  name: 'Eritrea',
  nationality: 'Eritrean'
},
{
  numCode: '233',
  codeTwoDigit: 'EE',
  codeThreeDigit: 'EST',
  name: 'Estonia',
  nationality: 'Estonian'
},
{
  numCode: '231',
  codeTwoDigit: 'ET',
  codeThreeDigit: 'ETH',
  name: 'Ethiopia',
  nationality: 'Ethiopian'
},
{
  numCode: '238',
  codeTwoDigit: 'FK',
  codeThreeDigit: 'FLK',
  name: 'Falkland Islands (Malvinas)',
  nationality: 'Falkland Island'
},
{
  numCode: '234',
  codeTwoDigit: 'FO',
  codeThreeDigit: 'FRO',
  name: 'Faroe Islands',
  nationality: 'Faroese'
},
{
  numCode: '242',
  codeTwoDigit: 'FJ',
  codeThreeDigit: 'FJI',
  name: 'Fiji',
  nationality: 'Fijian'
},
{
  numCode: '246',
  codeTwoDigit: 'FI',
  codeThreeDigit: 'FIN',
  name: 'Finland',
  nationality: 'Finnish'
},
{
  numCode: '250',
  codeTwoDigit: 'FR',
  codeThreeDigit: 'FRA',
  name: 'France',
  nationality: 'French'
},
{
  numCode: '254',
  codeTwoDigit: 'GF',
  codeThreeDigit: 'GUF',
  name: 'French Guiana',
  nationality: 'French Guianese'
},
{
  numCode: '258',
  codeTwoDigit: 'PF',
  codeThreeDigit: 'PYF',
  name: 'French Polynesia',
  nationality: 'French Polynesian'
},
{
  numCode: '260',
  codeTwoDigit: 'TF',
  codeThreeDigit: 'ATF',
  name: 'French Southern Territories',
  nationality: 'French Southern Territories'
},
{
  numCode: '266',
  codeTwoDigit: 'GA',
  codeThreeDigit: 'GAB',
  name: 'Gabon',
  nationality: 'Gabonese'
},
{
  numCode: '270',
  codeTwoDigit: 'GM',
  codeThreeDigit: 'GMB',
  name: 'Gambia',
  nationality: 'Gambian'
},
{
  numCode: '268',
  codeTwoDigit: 'GE',
  codeThreeDigit: 'GEO',
  name: 'Georgia',
  nationality: 'Georgian'
},
{
  numCode: '276',
  codeTwoDigit: 'DE',
  codeThreeDigit: 'DEU',
  name: 'Germany',
  nationality: 'German'
},
{
  numCode: '288',
  codeTwoDigit: 'GH',
  codeThreeDigit: 'GHA',
  name: 'Ghana',
  nationality: 'Ghanaian'
},
{
  numCode: '292',
  codeTwoDigit: 'GI',
  codeThreeDigit: 'GIB',
  name: 'Gibraltar',
  nationality: 'Gibraltar'
},
{
  numCode: '300',
  codeTwoDigit: 'GR',
  codeThreeDigit: 'GRC',
  name: 'Greece',
  nationality: 'Greek, Hellenic'
},
{
  numCode: '304',
  codeTwoDigit: 'GL',
  codeThreeDigit: 'GRL',
  name: 'Greenland',
  nationality: 'Greenlandic'
},
{
  numCode: '308',
  codeTwoDigit: 'GD',
  codeThreeDigit: 'GRD',
  name: 'Grenada',
  nationality: 'Grenadian'
},
{
  numCode: '312',
  codeTwoDigit: 'GP',
  codeThreeDigit: 'GLP',
  name: 'Guadeloupe',
  nationality: 'Guadeloupe'
},
{
  numCode: '316',
  codeTwoDigit: 'GU',
  codeThreeDigit: 'GUM',
  name: 'Guam',
  nationality: 'Guamanian, Guambat'
},
{
  numCode: '320',
  codeTwoDigit: 'GT',
  codeThreeDigit: 'GTM',
  name: 'Guatemala',
  nationality: 'Guatemalan'
},
{
  numCode: '831',
  codeTwoDigit: 'GG',
  codeThreeDigit: 'GGY',
  name: 'Guernsey',
  nationality: 'Channel Island'
},
{
  numCode: '324',
  codeTwoDigit: 'GN',
  codeThreeDigit: 'GIN',
  name: 'Guinea',
  nationality: 'Guinean'
},
{
  numCode: '624',
  codeTwoDigit: 'GW',
  codeThreeDigit: 'GNB',
  name: 'Guinea-Bissau',
  nationality: 'Bissau-Guinean'
},
{
  numCode: '328',
  codeTwoDigit: 'GY',
  codeThreeDigit: 'GUY',
  name: 'Guyana',
  nationality: 'Guyanese'
},
{
  numCode: '332',
  codeTwoDigit: 'HT',
  codeThreeDigit: 'HTI',
  name: 'Haiti',
  nationality: 'Haitian'
},
{
  numCode: '334',
  codeTwoDigit: 'HM',
  codeThreeDigit: 'HMD',
  name: 'Heard Island and McDonald Islands',
  nationality: 'Heard Island or McDonald Islands'
},
{
  numCode: '336',
  codeTwoDigit: 'VA',
  codeThreeDigit: 'VAT',
  name: 'Vatican City State',
  nationality: 'Vatican'
},
{
  numCode: '340',
  codeTwoDigit: 'HN',
  codeThreeDigit: 'HND',
  name: 'Honduras',
  nationality: 'Honduran'
},
{
  numCode: '344',
  codeTwoDigit: 'HK',
  codeThreeDigit: 'HKG',
  name: 'Hong Kong',
  nationality: 'Hong Kong, Hong Kongese'
},
{
  numCode: '348',
  codeTwoDigit: 'HU',
  codeThreeDigit: 'HUN',
  name: 'Hungary',
  nationality: 'Hungarian, Magyar'
},
{
  numCode: '352',
  codeTwoDigit: 'IS',
  codeThreeDigit: 'ISL',
  name: 'Iceland',
  nationality: 'Icelandic'
},
{
  numCode: '356',
  codeTwoDigit: 'IN',
  codeThreeDigit: 'IND',
  name: 'India',
  nationality: 'Indian'
},
{
  numCode: '360',
  codeTwoDigit: 'ID',
  codeThreeDigit: 'IDN',
  name: 'Indonesia',
  nationality: 'Indonesian'
},
{
  numCode: '364',
  codeTwoDigit: 'IR',
  codeThreeDigit: 'IRN',
  name: 'Iran',
  nationality: 'Iranian, Persian'
},
{
  numCode: '368',
  codeTwoDigit: 'IQ',
  codeThreeDigit: 'IRQ',
  name: 'Iraq',
  nationality: 'Iraqi'
},
{
  numCode: '372',
  codeTwoDigit: 'IE',
  codeThreeDigit: 'IRL',
  name: 'Ireland',
  nationality: 'Irish'
},
{
  numCode: '833',
  codeTwoDigit: 'IM',
  codeThreeDigit: 'IMN',
  name: 'Isle of Man',
  nationality: 'Manx'
},
{
  numCode: '380',
  codeTwoDigit: 'IT',
  codeThreeDigit: 'ITA',
  name: 'Italy',
  nationality: 'Italian'
},
{
  numCode: '388',
  codeTwoDigit: 'JM',
  codeThreeDigit: 'JAM',
  name: 'Jamaica',
  nationality: 'Jamaican'
},
{
  numCode: '392',
  codeTwoDigit: 'JP',
  codeThreeDigit: 'JPN',
  name: 'Japan',
  nationality: 'Japanese'
},
{
  numCode: '400',
  codeTwoDigit: 'JO',
  codeThreeDigit: 'JOR',
  name: 'Jordan',
  nationality: 'Jordanian'
},
{
  numCode: '398',
  codeTwoDigit: 'KZ',
  codeThreeDigit: 'KAZ',
  name: 'Kazakhstan',
  nationality: 'Kazakhstani, Kazakh'
},
{
  numCode: '404',
  codeTwoDigit: 'KE',
  codeThreeDigit: 'KEN',
  name: 'Kenya',
  nationality: 'Kenyan'
},
{
  numCode: '296',
  codeTwoDigit: 'KI',
  codeThreeDigit: 'KIR',
  name: 'Kiribati',
  nationality: 'I-Kiribati'
},
{
  numCode: '408',
  codeTwoDigit: 'KP',
  codeThreeDigit: 'PRK',
  name: 'Korea (Democratic People\'s Republic of)',
  nationality: 'North Korean'
},
{
  numCode: '410',
  codeTwoDigit: 'KR',
  codeThreeDigit: 'KOR',
  name: 'Korea (Republic of)',
  nationality: 'South Korean'
},
{
  numCode: '414',
  codeTwoDigit: 'KW',
  codeThreeDigit: 'KWT',
  name: 'Kuwait',
  nationality: 'Kuwaiti'
},
{
  numCode: '417',
  codeTwoDigit: 'KG',
  codeThreeDigit: 'KGZ',
  name: 'Kyrgyzstan',
  nationality: 'Kyrgyzstani, Kyrgyz, Kirgiz, Kirghiz'
},
{
  numCode: '418',
  codeTwoDigit: 'LA',
  codeThreeDigit: 'LAO',
  name: 'Lao People\'s Democratic Republic',
  nationality: 'Lao, Laotian'
},
{
  numCode: '428',
  codeTwoDigit: 'LV',
  codeThreeDigit: 'LVA',
  name: 'Latvia',
  nationality: 'Latvian'
},
{
  numCode: '422',
  codeTwoDigit: 'LB',
  codeThreeDigit: 'LBN',
  name: 'Lebanon',
  nationality: 'Lebanese'
},
{
  numCode: '426',
  codeTwoDigit: 'LS',
  codeThreeDigit: 'LSO',
  name: 'Lesotho',
  nationality: 'Basotho'
},
{
  numCode: '430',
  codeTwoDigit: 'LR',
  codeThreeDigit: 'LBR',
  name: 'Liberia',
  nationality: 'Liberian'
},
{
  numCode: '434',
  codeTwoDigit: 'LY',
  codeThreeDigit: 'LBY',
  name: 'Libya',
  nationality: 'Libyan'
},
{
  numCode: '438',
  codeTwoDigit: 'LI',
  codeThreeDigit: 'LIE',
  name: 'Liechtenstein',
  nationality: 'Liechtenstein'
},
{
  numCode: '440',
  codeTwoDigit: 'LT',
  codeThreeDigit: 'LTU',
  name: 'Lithuania',
  nationality: 'Lithuanian'
},
{
  numCode: '442',
  codeTwoDigit: 'LU',
  codeThreeDigit: 'LUX',
  name: 'Luxembourg',
  nationality: 'Luxembourg, Luxembourgish'
},
{
  numCode: '446',
  codeTwoDigit: 'MO',
  codeThreeDigit: 'MAC',
  name: 'Macao',
  nationality: 'Macanese, Chinese'
},
{
  numCode: '807',
  codeTwoDigit: 'MK',
  codeThreeDigit: 'MKD',
  name: 'Macedonia (the former Yugoslav Republic of)',
  nationality: 'Macedonian'
},
{
  numCode: '450',
  codeTwoDigit: 'MG',
  codeThreeDigit: 'MDG',
  name: 'Madagascar',
  nationality: 'Malagasy'
},
{
  numCode: '454',
  codeTwoDigit: 'MW',
  codeThreeDigit: 'MWI',
  name: 'Malawi',
  nationality: 'Malawian'
},
{
  numCode: '458',
  codeTwoDigit: 'MY',
  codeThreeDigit: 'MYS',
  name: 'Malaysia',
  nationality: 'Malaysian'
},
{
  numCode: '462',
  codeTwoDigit: 'MV',
  codeThreeDigit: 'MDV',
  name: 'Maldives',
  nationality: 'Maldivian'
},
{
  numCode: '466',
  codeTwoDigit: 'ML',
  codeThreeDigit: 'MLI',
  name: 'Mali',
  nationality: 'Malian, Malinese'
},
{
  numCode: '470',
  codeTwoDigit: 'MT',
  codeThreeDigit: 'MLT',
  name: 'Malta',
  nationality: 'Maltese'
},
{
  numCode: '584',
  codeTwoDigit: 'MH',
  codeThreeDigit: 'MHL',
  name: 'Marshall Islands',
  nationality: 'Marshallese'
},
{
  numCode: '474',
  codeTwoDigit: 'MQ',
  codeThreeDigit: 'MTQ',
  name: 'Martinique',
  nationality: 'Martiniquais, Martinican'
},
{
  numCode: '478',
  codeTwoDigit: 'MR',
  codeThreeDigit: 'MRT',
  name: 'Mauritania',
  nationality: 'Mauritanian'
},
{
  numCode: '480',
  codeTwoDigit: 'MU',
  codeThreeDigit: 'MUS',
  name: 'Mauritius',
  nationality: 'Mauritian'
},
{
  numCode: '175',
  codeTwoDigit: 'YT',
  codeThreeDigit: 'MYT',
  name: 'Mayotte',
  nationality: 'Mahoran'
},
{
  numCode: '484',
  codeTwoDigit: 'MX',
  codeThreeDigit: 'MEX',
  name: 'Mexico',
  nationality: 'Mexican'
},
{
  numCode: '583',
  codeTwoDigit: 'FM',
  codeThreeDigit: 'FSM',
  name: 'Micronesia (Federated States of)',
  nationality: 'Micronesian'
},
{
  numCode: '498',
  codeTwoDigit: 'MD',
  codeThreeDigit: 'MDA',
  name: 'Moldova (Republic of)',
  nationality: 'Moldovan'
},
{
  numCode: '492',
  codeTwoDigit: 'MC',
  codeThreeDigit: 'MCO',
  name: 'Monaco',
  nationality: 'Mon\u00e9gasque, Monacan'
},
{
  numCode: '496',
  codeTwoDigit: 'MN',
  codeThreeDigit: 'MNG',
  name: 'Mongolia',
  nationality: 'Mongolian'
},
{
  numCode: '499',
  codeTwoDigit: 'ME',
  codeThreeDigit: 'MNE',
  name: 'Montenegro',
  nationality: 'Montenegrin'
},
{
  numCode: '500',
  codeTwoDigit: 'MS',
  codeThreeDigit: 'MSR',
  name: 'Montserrat',
  nationality: 'Montserratian'
},
{
  numCode: '504',
  codeTwoDigit: 'MA',
  codeThreeDigit: 'MAR',
  name: 'Morocco',
  nationality: 'Moroccan'
},
{
  numCode: '508',
  codeTwoDigit: 'MZ',
  codeThreeDigit: 'MOZ',
  name: 'Mozambique',
  nationality: 'Mozambican'
},
{
  numCode: '104',
  codeTwoDigit: 'MM',
  codeThreeDigit: 'MMR',
  name: 'Myanmar',
  nationality: 'Burmese'
},
{
  numCode: '516',
  codeTwoDigit: 'NA',
  codeThreeDigit: 'NAM',
  name: 'Namibia',
  nationality: 'Namibian'
},
{
  numCode: '520',
  codeTwoDigit: 'NR',
  codeThreeDigit: 'NRU',
  name: 'Nauru',
  nationality: 'Nauruan'
},
{
  numCode: '524',
  codeTwoDigit: 'NP',
  codeThreeDigit: 'NPL',
  name: 'Nepal',
  nationality: 'Nepali, Nepalese'
},
{
  numCode: '528',
  codeTwoDigit: 'NL',
  codeThreeDigit: 'NLD',
  name: 'Netherlands',
  nationality: 'Dutch, Netherlandic'
},
{
  numCode: '540',
  codeTwoDigit: 'NC',
  codeThreeDigit: 'NCL',
  name: 'New Caledonia',
  nationality: 'New Caledonian'
},
{
  numCode: '554',
  codeTwoDigit: 'NZ',
  codeThreeDigit: 'NZL',
  name: 'New Zealand',
  nationality: 'New Zealand, NZ'
},
{
  numCode: '558',
  codeTwoDigit: 'NI',
  codeThreeDigit: 'NIC',
  name: 'Nicaragua',
  nationality: 'Nicaraguan'
},
{
  numCode: '562',
  codeTwoDigit: 'NE',
  codeThreeDigit: 'NER',
  name: 'Niger',
  nationality: 'Nigerien'
},
{
  numCode: '566',
  codeTwoDigit: 'NG',
  codeThreeDigit: 'NGA',
  name: 'Nigeria',
  nationality: 'Nigerian'
},
{
  numCode: '570',
  codeTwoDigit: 'NU',
  codeThreeDigit: 'NIU',
  name: 'Niue',
  nationality: 'Niuean'
},
{
  numCode: '574',
  codeTwoDigit: 'NF',
  codeThreeDigit: 'NFK',
  name: 'Norfolk Island',
  nationality: 'Norfolk Island'
},
{
  numCode: '580',
  codeTwoDigit: 'MP',
  codeThreeDigit: 'MNP',
  name: 'Northern Mariana Islands',
  nationality: 'Northern Marianan'
},
{
  numCode: '578',
  codeTwoDigit: 'NO',
  codeThreeDigit: 'NOR',
  name: 'Norway',
  nationality: 'Norwegian'
},
{
  numCode: '512',
  codeTwoDigit: 'OM',
  codeThreeDigit: 'OMN',
  name: 'Oman',
  nationality: 'Omani'
},
{
  numCode: '586',
  codeTwoDigit: 'PK',
  codeThreeDigit: 'PAK',
  name: 'Pakistan',
  nationality: 'Pakistani'
},
{
  numCode: '585',
  codeTwoDigit: 'PW',
  codeThreeDigit: 'PLW',
  name: 'Palau',
  nationality: 'Palauan'
},
{
  numCode: '275',
  codeTwoDigit: 'PS',
  codeThreeDigit: 'PSE',
  name: 'Palestine, State of',
  nationality: 'Palestinian'
},
{
  numCode: '591',
  codeTwoDigit: 'PA',
  codeThreeDigit: 'PAN',
  name: 'Panama',
  nationality: 'Panamanian'
},
{
  numCode: '598',
  codeTwoDigit: 'PG',
  codeThreeDigit: 'PNG',
  name: 'Papua New Guinea',
  nationality: 'Papua New Guinean, Papuan'
},
{
  numCode: '600',
  codeTwoDigit: 'PY',
  codeThreeDigit: 'PRY',
  name: 'Paraguay',
  nationality: 'Paraguayan'
},
{
  numCode: '604',
  codeTwoDigit: 'PE',
  codeThreeDigit: 'PER',
  name: 'Peru',
  nationality: 'Peruvian'
},
{
  numCode: '608',
  codeTwoDigit: 'PH',
  codeThreeDigit: 'PHL',
  name: 'Philippines',
  nationality: 'Philippine, Filipino'
},
{
  numCode: '612',
  codeTwoDigit: 'PN',
  codeThreeDigit: 'PCN',
  name: 'Pitcairn',
  nationality: 'Pitcairn Island'
},
{
  numCode: '616',
  codeTwoDigit: 'PL',
  codeThreeDigit: 'POL',
  name: 'Poland',
  nationality: 'Polish'
},
{
  numCode: '620',
  codeTwoDigit: 'PT',
  codeThreeDigit: 'PRT',
  name: 'Portugal',
  nationality: 'Portuguese'
},
{
  numCode: '630',
  codeTwoDigit: 'PR',
  codeThreeDigit: 'PRI',
  name: 'Puerto Rico',
  nationality: 'Puerto Rican'
},
{
  numCode: '634',
  codeTwoDigit: 'QA',
  codeThreeDigit: 'QAT',
  name: 'Qatar',
  nationality: 'Qatari'
},
{
  numCode: '638',
  codeTwoDigit: 'RE',
  codeThreeDigit: 'REU',
  name: 'R\u00e9union',
  nationality: 'R\u00e9unionese, R\u00e9unionnais'
},
{
  numCode: '642',
  codeTwoDigit: 'RO',
  codeThreeDigit: 'ROU',
  name: 'Romania',
  nationality: 'Romanian'
},
{
  numCode: '643',
  codeTwoDigit: 'RU',
  codeThreeDigit: 'RUS',
  name: 'Russian Federation',
  nationality: 'Russian'
},
{
  numCode: '646',
  codeTwoDigit: 'RW',
  codeThreeDigit: 'RWA',
  name: 'Rwanda',
  nationality: 'Rwandan'
},
{
  numCode: '652',
  codeTwoDigit: 'BL',
  codeThreeDigit: 'BLM',
  name: 'Saint Barth\u00e9lemy',
  nationality: 'Barth\u00e9lemois'
},
{
  numCode: '654',
  codeTwoDigit: 'SH',
  codeThreeDigit: 'SHN',
  name: 'Saint Helena, Ascension and Tristan da Cunha',
  nationality: 'Saint Helenian'
},
{
  numCode: '659',
  codeTwoDigit: 'KN',
  codeThreeDigit: 'KNA',
  name: 'Saint Kitts and Nevis',
  nationality: 'Kittitian or Nevisian'
},
{
  numCode: '662',
  codeTwoDigit: 'LC',
  codeThreeDigit: 'LCA',
  name: 'Saint Lucia',
  nationality: 'Saint Lucian'
},
{
  numCode: '663',
  codeTwoDigit: 'MF',
  codeThreeDigit: 'MAF',
  name: 'Saint Martin (French part)',
  nationality: 'Saint-Martinoise'
},
{
  numCode: '666',
  codeTwoDigit: 'PM',
  codeThreeDigit: 'SPM',
  name: 'Saint Pierre and Miquelon',
  nationality: 'Saint-Pierrais or Miquelonnais'
},
{
  numCode: '670',
  codeTwoDigit: 'VC',
  codeThreeDigit: 'VCT',
  name: 'Saint Vincent and the Grenadines',
  nationality: 'Saint Vincentian, Vincentian'
},
{
  numCode: '882',
  codeTwoDigit: 'WS',
  codeThreeDigit: 'WSM',
  name: 'Samoa',
  nationality: 'Samoan'
},
{
  numCode: '674',
  codeTwoDigit: 'SM',
  codeThreeDigit: 'SMR',
  name: 'San Marino',
  nationality: 'Sammarinese'
},
{
  numCode: '678',
  codeTwoDigit: 'ST',
  codeThreeDigit: 'STP',
  name: 'Sao Tome and Principe',
  nationality: 'S\u00e3o Tom\u00e9an'
},
{
  numCode: '682',
  codeTwoDigit: 'SA',
  codeThreeDigit: 'SAU',
  name: 'Saudi Arabia',
  nationality: 'Saudi, Saudi Arabian'
},
{
  numCode: '686',
  codeTwoDigit: 'SN',
  codeThreeDigit: 'SEN',
  name: 'Senegal',
  nationality: 'Senegalese'
},
{
  numCode: '688',
  codeTwoDigit: 'RS',
  codeThreeDigit: 'SRB',
  name: 'Serbia',
  nationality: 'Serbian'
},
{
  numCode: '690',
  codeTwoDigit: 'SC',
  codeThreeDigit: 'SYC',
  name: 'Seychelles',
  nationality: 'Seychellois'
},
{
  numCode: '694',
  codeTwoDigit: 'SL',
  codeThreeDigit: 'SLE',
  name: 'Sierra Leone',
  nationality: 'Sierra Leonean'
},
{
  numCode: '702',
  codeTwoDigit: 'SG',
  codeThreeDigit: 'SGP',
  name: 'Singapore',
  nationality: 'Singaporean'
},
{
  numCode: '534',
  codeTwoDigit: 'SX',
  codeThreeDigit: 'SXM',
  name: 'Sint Maarten (Dutch part)',
  nationality: 'Sint Maarten'
},
{
  numCode: '703',
  codeTwoDigit: 'SK',
  codeThreeDigit: 'SVK',
  name: 'Slovakia',
  nationality: 'Slovak'
},
{
  numCode: '705',
  codeTwoDigit: 'SI',
  codeThreeDigit: 'SVN',
  name: 'Slovenia',
  nationality: 'Slovenian, Slovene'
},
{
  numCode: '90',
  codeTwoDigit: 'SB',
  codeThreeDigit: 'SLB',
  name: 'Solomon Islands',
  nationality: 'Solomon Island'
},
{
  numCode: '706',
  codeTwoDigit: 'SO',
  codeThreeDigit: 'SOM',
  name: 'Somalia',
  nationality: 'Somali, Somalian'
},
{
  numCode: '710',
  codeTwoDigit: 'ZA',
  codeThreeDigit: 'ZAF',
  name: 'South Africa',
  nationality: 'South African'
},
{
  numCode: '239',
  codeTwoDigit: 'GS',
  codeThreeDigit: 'SGS',
  name: 'South Georgia and the South Sandwich Islands',
  nationality: 'South Georgia or South Sandwich Islands'
},
{
  numCode: '728',
  codeTwoDigit: 'SS',
  codeThreeDigit: 'SSD',
  name: 'South Sudan',
  nationality: 'South Sudanese'
},
{
  numCode: '724',
  codeTwoDigit: 'ES',
  codeThreeDigit: 'ESP',
  name: 'Spain',
  nationality: 'Spanish'
},
{
  numCode: '144',
  codeTwoDigit: 'LK',
  codeThreeDigit: 'LKA',
  name: 'Sri Lanka',
  nationality: 'Sri Lankan'
},
{
  numCode: '729',
  codeTwoDigit: 'SD',
  codeThreeDigit: 'SDN',
  name: 'Sudan',
  nationality: 'Sudanese'
},
{
  numCode: '740',
  codeTwoDigit: 'SR',
  codeThreeDigit: 'SUR',
  name: 'Suriname',
  nationality: 'Surinamese'
},
{
  numCode: '744',
  codeTwoDigit: 'SJ',
  codeThreeDigit: 'SJM',
  name: 'Svalbard and Jan Mayen',
  nationality: 'Svalbard'
},
{
  numCode: '748',
  codeTwoDigit: 'SZ',
  codeThreeDigit: 'SWZ',
  name: 'Swaziland',
  nationality: 'Swazi'
},
{
  numCode: '752',
  codeTwoDigit: 'SE',
  codeThreeDigit: 'SWE',
  name: 'Sweden',
  nationality: 'Swedish'
},
{
  numCode: '756',
  codeTwoDigit: 'CH',
  codeThreeDigit: 'CHE',
  name: 'Switzerland',
  nationality: 'Swiss'
},
{
  numCode: '760',
  codeTwoDigit: 'SY',
  codeThreeDigit: 'SYR',
  name: 'Syrian Arab Republic',
  nationality: 'Syrian'
},
{
  numCode: '158',
  codeTwoDigit: 'TW',
  codeThreeDigit: 'TWN',
  name: 'Taiwan, Province of China',
  nationality: 'Chinese, Taiwanese'
},
{
  numCode: '762',
  codeTwoDigit: 'TJ',
  codeThreeDigit: 'TJK',
  name: 'Tajikistan',
  nationality: 'Tajikistani'
},
{
  numCode: '834',
  codeTwoDigit: 'TZ',
  codeThreeDigit: 'TZA',
  name: 'Tanzania, United Republic of',
  nationality: 'Tanzanian'
},
{
  numCode: '764',
  codeTwoDigit: 'TH',
  codeThreeDigit: 'THA',
  name: 'Thailand',
  nationality: 'Thai'
},
{
  numCode: '626',
  codeTwoDigit: 'TL',
  codeThreeDigit: 'TLS',
  name: 'Timor-Leste',
  nationality: 'Timorese'
},
{
  numCode: '768',
  codeTwoDigit: 'TG',
  codeThreeDigit: 'TGO',
  name: 'Togo',
  nationality: 'Togolese'
},
{
  numCode: '772',
  codeTwoDigit: 'TK',
  codeThreeDigit: 'TKL',
  name: 'Tokelau',
  nationality: 'Tokelauan'
},
{
  numCode: '776',
  codeTwoDigit: 'TO',
  codeThreeDigit: 'TON',
  name: 'Tonga',
  nationality: 'Tongan'
},
{
  numCode: '780',
  codeTwoDigit: 'TT',
  codeThreeDigit: 'TTO',
  name: 'Trinidad and Tobago',
  nationality: 'Trinidadian or Tobagonian'
},
{
  numCode: '788',
  codeTwoDigit: 'TN',
  codeThreeDigit: 'TUN',
  name: 'Tunisia',
  nationality: 'Tunisian'
},
{
  numCode: '792',
  codeTwoDigit: 'TR',
  codeThreeDigit: 'TUR',
  name: 'Turkey',
  nationality: 'Turkish'
},
{
  numCode: '795',
  codeTwoDigit: 'TM',
  codeThreeDigit: 'TKM',
  name: 'Turkmenistan',
  nationality: 'Turkmen'
},
{
  numCode: '796',
  codeTwoDigit: 'TC',
  codeThreeDigit: 'TCA',
  name: 'Turks and Caicos Islands',
  nationality: 'Turks and Caicos Island'
},
{
  numCode: '798',
  codeTwoDigit: 'TV',
  codeThreeDigit: 'TUV',
  name: 'Tuvalu',
  nationality: 'Tuvaluan'
},
{
  numCode: '800',
  codeTwoDigit: 'UG',
  codeThreeDigit: 'UGA',
  name: 'Uganda',
  nationality: 'Ugandan'
},
{
  numCode: '804',
  codeTwoDigit: 'UA',
  codeThreeDigit: 'UKR',
  name: 'Ukraine',
  nationality: 'Ukrainian'
},
{
  numCode: '784',
  codeTwoDigit: 'AE',
  codeThreeDigit: 'ARE',
  name: 'United Arab Emirates',
  nationality: 'Emirati, Emirian, Emiri'
},
{
  numCode: '826',
  codeTwoDigit: 'GB',
  codeThreeDigit: 'GBR',
  name: 'United Kingdom of Great Britain and Northern Ireland',
  nationality: 'British, UK'
},
{
  numCode: '840',
  codeTwoDigit: 'US',
  codeThreeDigit: 'USA',
  name: 'United States of America',
  nationality: 'American'
},
{
  numCode: '858',
  codeTwoDigit: 'UY',
  codeThreeDigit: 'URY',
  name: 'Uruguay',
  nationality: 'Uruguayan'
},
{
  numCode: '860',
  codeTwoDigit: 'UZ',
  codeThreeDigit: 'UZB',
  name: 'Uzbekistan',
  nationality: 'Uzbekistani, Uzbek'
},
{
  numCode: '548',
  codeTwoDigit: 'VU',
  codeThreeDigit: 'VUT',
  name: 'Vanuatu',
  nationality: 'Ni-Vanuatu, Vanuatuan'
},
{
  numCode: '862',
  codeTwoDigit: 'VE',
  codeThreeDigit: 'VEN',
  name: 'Venezuela (Bolivarian Republic of)',
  nationality: 'Venezuelan'
},
{
  numCode: '704',
  codeTwoDigit: 'VN',
  codeThreeDigit: 'VNM',
  name: 'Vietnam',
  nationality: 'Vietnamese'
},
{
  numCode: '92',
  codeTwoDigit: 'VG',
  codeThreeDigit: 'VGB',
  name: 'Virgin Islands (British)',
  nationality: 'British Virgin Island'
},
{
  numCode: '850',
  codeTwoDigit: 'VI',
  codeThreeDigit: 'VIR',
  name: 'Virgin Islands (U.S.)',
  nationality: 'U.S. Virgin Island'
},
{
  numCode: '876',
  codeTwoDigit: 'WF',
  codeThreeDigit: 'WLF',
  name: 'Wallis and Futuna',
  nationality: 'Wallis and Futuna, Wallisian or Futunan'
},
{
  numCode: '732',
  codeTwoDigit: 'EH',
  codeThreeDigit: 'ESH',
  name: 'Western Sahara',
  nationality: 'Sahrawi, Sahrawian, Sahraouian'
},
{
  numCode: '887',
  codeTwoDigit: 'YE',
  codeThreeDigit: 'YEM',
  name: 'Yemen',
  nationality: 'Yemeni'
},
{
  numCode: '894',
  codeTwoDigit: 'ZM',
  codeThreeDigit: 'ZMB',
  name: 'Zambia',
  nationality: 'Zambian'
},
{
  numCode: '716',
  codeTwoDigit: 'ZW',
  codeThreeDigit: 'ZWE',
  name: 'Zimbabwe',
  nationality: 'Zimbabwean'
}
];


export const COUNTRIES: Countries[] = [
{
  numCode: '414',
  codeTwoDigit: 'kw',
  codeThreeDigit: 'KWT',
  name: 'Kuwait'
},
{
  numCode: '682',
  codeTwoDigit: 'sa',
  codeThreeDigit: 'SAU',
  name: 'KSA'
},
{
  numCode: '784',
  codeTwoDigit: 'ae',
  codeThreeDigit: 'ARE',
  name: 'UAE'
},
];

export const COUNTRIESAR: Countries[] = [{
  numCode: '414',
  codeTwoDigit: 'kw',
  codeThreeDigit: 'KWT',
  name: 'الكويت'
},
{
  numCode: '682',
  codeTwoDigit: 'sa',
  codeThreeDigit: 'SAU',
  name: 'المملكة العربية السعودية'
},
{
  numCode: '784',
  codeTwoDigit: 'ae',
  codeThreeDigit: 'ARE',
  name: 'الإمارات العربية المتحدة'
},
];

export const NATIONALITIESAR: Countries[] = [
  {
      numCode: 'AD',
      nationality: 'أندورا',
      dialCode: '+376'
  },
  {
      numCode: 'AE',
      nationality: 'الامارات العربية المتحدة',
      dialCode: '+971'
  },
  {
      numCode: 'AF',
      nationality: 'أفغانستان',
      dialCode: '+93'
  },
  {
      numCode: 'AG',
      nationality: 'أنتيجوا وبربودا',
      dialCode: '+1'
  },
  {
      numCode: 'AI',
      nationality: 'أنجويلا',
      dialCode: '+1'
  },
  {
      numCode: 'AL',
      nationality: 'ألبانيا',
      dialCode: '+355'
  },
  {
      numCode: 'AM',
      nationality: 'أرمينيا',
      dialCode: '+374'
  },
  {
      numCode: 'AO',
      nationality: 'أنجولا',
      dialCode: '+244'
  },
  {
      numCode: 'AQ',
      nationality: 'القطب الجنوبي',
      dialCode: '+672'
  },
  {
      numCode: 'AR',
      nationality: 'الأرجنتين',
      dialCode: '+54'
  },
  {
      numCode: 'AS',
      nationality: 'ساموا الأمريكية',
      dialCode: '+1'
  },
  {
      numCode: 'AT',
      nationality: 'النمسا',
      dialCode: '+43'
  },
  {
      numCode: 'AU',
      nationality: 'أستراليا',
      dialCode: '+61'
  },
  {
      numCode: 'AW',
      nationality: 'آروبا',
      dialCode: '+297'
  },
  {
      numCode: 'AX',
      nationality: 'جزر أولان',
      dialCode: '+358'
  },
  {
      numCode: 'AZ',
      nationality: 'أذربيجان',
      dialCode: '+994'
  },
  {
      numCode: 'BA',
      nationality: 'البوسنة والهرسك',
      dialCode: '+387'
  },
  {
      numCode: 'BB',
      nationality: 'بربادوس',
      dialCode: '+1'
  },
  {
      numCode: 'BD',
      nationality: 'بنجلاديش',
      dialCode: '+880'
  },
  {
      numCode: 'BE',
      nationality: 'بلجيكا',
      dialCode: '+32'
  },
  {
      numCode: 'BF',
      nationality: 'بوركينا فاسو',
      dialCode: '+226'
  },
  {
      numCode: 'BG',
      nationality: 'بلغاريا',
      dialCode: '+359'
  },
  {
      numCode: 'BH',
      nationality: 'البحرين',
      dialCode: '+973'
  },
  {
      numCode: 'BI',
      nationality: 'بوروندي',
      dialCode: '+257'
  },
  {
      numCode: 'BJ',
      nationality: 'بنين',
      dialCode: '+229'
  },
  {
      numCode: 'BL',
      nationality: 'سان بارتيلمي',
      dialCode: '+590'
  },
  {
      numCode: 'BM',
      nationality: 'برمودا',
      dialCode: '+1'
  },
  {
      numCode: 'BN',
      nationality: 'بروناي',
      dialCode: '+673'
  },
  {
      numCode: 'BO',
      nationality: 'بوليفيا',
      dialCode: '+591'
  },
  {
      numCode: 'BQ',
      nationality: 'بونير',
      dialCode: '+599'
  },
  {
      numCode: 'BR',
      nationality: 'البرازيل',
      dialCode: '+55'
  },
  {
      numCode: 'BS',
      nationality: 'الباهاما',
      dialCode: '+1'
  },
  {
      numCode: 'BT',
      nationality: 'بوتان',
      dialCode: '+975'
  },
  {
      numCode: 'BV',
      nationality: 'جزيرة بوفيه',
      dialCode: '+47'
  },
  {
      numCode: 'BW',
      nationality: 'بتسوانا',
      dialCode: '+267'
  },
  {
      numCode: 'BY',
      nationality: 'روسيا البيضاء',
      dialCode: '+375'
  },
  {
      numCode: 'BZ',
      nationality: 'بليز',
      dialCode: '+501'
  },
  {
      numCode: 'CA',
      nationality: 'كندا',
      dialCode: '+1'
  },
  {
      numCode: 'CC',
      nationality: 'جزر كوكوس',
      dialCode: '+61'
  },
  {
      numCode: 'CD',
      nationality: 'جمهورية الكونغو الديمقراطية',
      dialCode: '+243'
  },
  {
      numCode: 'CF',
      nationality: 'جمهورية افريقيا الوسطى',
      dialCode: '+236'
  },
  {
      numCode: 'CG',
      nationality: 'الكونغو - برازافيل',
      dialCode: '+242'
  },
  {
      numCode: 'CH',
      nationality: 'سويسرا',
      dialCode: '+41'
  },
  {
      numCode: 'CI',
      nationality: 'ساحل العاج',
      dialCode: '+225'
  },
  {
      numCode: 'CK',
      nationality: 'جزر كوك',
      dialCode: '+682'
  },
  {
      numCode: 'CL',
      nationality: 'شيلي',
      dialCode: '+56'
  },
  {
      numCode: 'CM',
      nationality: 'الكاميرون',
      dialCode: '+237'
  },
  {
      numCode: 'CN',
      nationality: 'الصين',
      dialCode: '+86'
  },
  {
      numCode: 'CO',
      nationality: 'كولومبيا',
      dialCode: '+57'
  },
  {
      numCode: 'CR',
      nationality: 'كوستاريكا',
      dialCode: '+506'
  },
  {
      numCode: 'CU',
      nationality: 'كوبا',
      dialCode: '+53'
  },
  {
      numCode: 'CV',
      nationality: 'الرأس الأخضر',
      dialCode: '+238'
  },
  {
      numCode: 'CW',
      nationality: 'كوراساو',
      dialCode: '+599'
  },
  {
      numCode: 'CX',
      nationality: 'جزيرة الكريسماس',
      dialCode: '+61'
  },
  {
      numCode: 'CY',
      nationality: 'قبرص',
      dialCode: '+357'
  },
  {
      numCode: 'CZ',
      nationality: 'جمهورية التشيك',
      dialCode: '+420'
  },
  {
      numCode: 'DE',
      nationality: 'ألمانيا',
      dialCode: '+49'
  },
  {
      numCode: 'DJ',
      nationality: 'جيبوتي',
      dialCode: '+253'
  },
  {
      numCode: 'DK',
      nationality: 'الدانمرك',
      dialCode: '+45'
  },
  {
      numCode: 'DM',
      nationality: 'دومينيكا',
      dialCode: '+1'
  },
  {
      numCode: 'DO',
      nationality: 'جمهورية الدومينيك',
      dialCode: '+1'
  },
  {
      numCode: 'DZ',
      nationality: 'الجزائر',
      dialCode: '+213'
  },
  {
      numCode: 'EC',
      nationality: 'الاكوادور',
      dialCode: '+593'
  },
  {
      numCode: 'EE',
      nationality: 'استونيا',
      dialCode: '+372'
  },
  {
      numCode: 'EG',
      nationality: 'مصر',
      dialCode: '+20'
  },
  {
      numCode: 'EH',
      nationality: 'الصحراء الغربية',
      dialCode: '+212'
  },
  {
      numCode: 'ER',
      nationality: 'اريتريا',
      dialCode: '+291'
  },
  {
      numCode: 'ES',
      nationality: 'أسبانيا',
      dialCode: '+34'
  },
  {
      numCode: 'ET',
      nationality: 'اثيوبيا',
      dialCode: '+251'
  },
  {
      numCode: 'FI',
      nationality: 'فنلندا',
      dialCode: '+358'
  },
  {
      numCode: 'FJ',
      nationality: 'فيجي',
      dialCode: '+679'
  },
  {
      numCode: 'FK',
      nationality: 'جزر فوكلاند',
      dialCode: '+500'
  },
  {
      numCode: 'FM',
      nationality: 'ميكرونيزيا',
      dialCode: '+691'
  },
  {
      numCode: 'FO',
      nationality: 'جزر فارو',
      dialCode: '+298'
  },
  {
      numCode: 'FR',
      nationality: 'فرنسا',
      dialCode: '+33'
  },
  {
      numCode: 'GA',
      nationality: 'الجابون',
      dialCode: '+241'
  },
  {
      numCode: 'GB',
      nationality: 'المملكة المتحدة',
      dialCode: '+44'
  },
  {
      numCode: 'GD',
      nationality: 'جرينادا',
      dialCode: '+1'
  },
  {
      numCode: 'GE',
      nationality: 'جورجيا',
      dialCode: '+995'
  },
  {
      numCode: 'GF',
      nationality: 'غويانا',
      dialCode: '+594'
  },
  {
      numCode: 'GG',
      nationality: 'غيرنزي',
      dialCode: '+44'
  },
  {
      numCode: 'GH',
      nationality: 'غانا',
      dialCode: '+233'
  },
  {
      numCode: 'GI',
      nationality: 'جبل طارق',
      dialCode: '+350'
  },
  {
      numCode: 'GL',
      nationality: 'جرينلاند',
      dialCode: '+299'
  },
  {
      numCode: 'GM',
      nationality: 'غامبيا',
      dialCode: '+220'
  },
  {
      numCode: 'GN',
      nationality: 'غينيا',
      dialCode: '+224'
  },
  {
      numCode: 'GP',
      nationality: 'جوادلوب',
      dialCode: '+590'
  },
  {
      numCode: 'GQ',
      nationality: 'غينيا الاستوائية',
      dialCode: '+240'
  },
  {
      numCode: 'GR',
      nationality: 'اليونان',
      dialCode: '+30'
  },
  {
      numCode: 'GS',
      nationality: 'جورجيا الجنوبية وجزر ساندويتش الجنوبية',
      dialCode: '+500'
  },
  {
      numCode: 'GT',
      nationality: 'جواتيمالا',
      dialCode: '+502'
  },
  {
      numCode: 'GU',
      nationality: 'جوام',
      dialCode: '+1'
  },
  {
      numCode: 'GW',
      nationality: 'غينيا بيساو',
      dialCode: '+245'
  },
  {
      numCode: 'GY',
      nationality: 'غيانا',
      dialCode: '+595'
  },
  {
      numCode: 'HK',
      nationality: 'هونج كونج الصينية',
      dialCode: '+852'
  },
  {
      numCode: 'HM',
      nationality: 'جزيرة هيرد وماكدونالد',
      dialCode: ''
  },
  {
      numCode: 'HN',
      nationality: 'هندوراس',
      dialCode: '+504'
  },
  {
      numCode: 'HR',
      nationality: 'كرواتيا',
      dialCode: '+385'
  },
  {
      numCode: 'HT',
      nationality: 'هايتي',
      dialCode: '+509'
  },
  {
      numCode: 'HU',
      nationality: 'المجر',
      dialCode: '+36'
  },
  {
      numCode: 'ID',
      nationality: 'اندونيسيا',
      dialCode: '+62'
  },
  {
      numCode: 'IE',
      nationality: 'أيرلندا',
      dialCode: '+353'
  },
  {
      numCode: 'IM',
      nationality: 'جزيرة مان',
      dialCode: '+44'
  },
  {
      numCode: 'IN',
      nationality: 'الهند',
      dialCode: '+91'
  },
  {
      numCode: 'IO',
      nationality: 'المحيط الهندي البريطاني',
      dialCode: '+246'
  },
  {
      numCode: 'IQ',
      nationality: 'العراق',
      dialCode: '+964'
  },
  {
      numCode: 'IR',
      nationality: 'ايران',
      dialCode: '+98'
  },
  {
      numCode: 'IS',
      nationality: 'أيسلندا',
      dialCode: '+354'
  },
  {
      numCode: 'IT',
      nationality: 'ايطاليا',
      dialCode: '+39'
  },
  {
      numCode: 'JE',
      nationality: 'جيرسي',
      dialCode: '+44'
  },
  {
      numCode: 'JM',
      nationality: 'جامايكا',
      dialCode: '+1'
  },
  {
      numCode: 'JO',
      nationality: 'الأردن',
      dialCode: '+962'
  },
  {
      numCode: 'JP',
      nationality: 'اليابان',
      dialCode: '+81'
  },
  {
      numCode: 'KE',
      nationality: 'كينيا',
      dialCode: '+254'
  },
  {
      numCode: 'KG',
      nationality: 'قرغيزستان',
      dialCode: '+996'
  },
  {
      numCode: 'KH',
      nationality: 'كمبوديا',
      dialCode: '+855'
  },
  {
      numCode: 'KI',
      nationality: 'كيريباتي',
      dialCode: '+686'
  },
  {
      numCode: 'KM',
      nationality: 'جزر القمر',
      dialCode: '+269'
  },
  {
      numCode: 'KN',
      nationality: 'سانت كيتس ونيفيس',
      dialCode: '+1'
  },
  {
      numCode: 'KP',
      nationality: 'كوريا الشمالية',
      dialCode: '+850'
  },
  {
      numCode: 'KR',
      nationality: 'كوريا الجنوبية',
      dialCode: '+82'
  },
  {
      numCode: 'KW',
      nationality: 'الكويت',
      dialCode: '+965'
  },
  {
      numCode: 'KY',
      nationality: 'جزر الكايمن',
      dialCode: '+345'
  },
  {
      numCode: 'KZ',
      nationality: 'كازاخستان',
      dialCode: '+7'
  },
  {
      numCode: 'LA',
      nationality: 'لاوس',
      dialCode: '+856'
  },
  {
      numCode: 'LB',
      nationality: 'لبنان',
      dialCode: '+961'
  },
  {
      numCode: 'LC',
      nationality: 'سانت لوسيا',
      dialCode: '+1'
  },
  {
      numCode: 'LI',
      nationality: 'ليختنشتاين',
      dialCode: '+423'
  },
  {
      numCode: 'LK',
      nationality: 'سريلانكا',
      dialCode: '+94'
  },
  {
      numCode: 'LR',
      nationality: 'ليبيريا',
      dialCode: '+231'
  },
  {
      numCode: 'LS',
      nationality: 'ليسوتو',
      dialCode: '+266'
  },
  {
      numCode: 'LT',
      nationality: 'ليتوانيا',
      dialCode: '+370'
  },
  {
      numCode: 'LU',
      nationality: 'لوكسمبورج',
      dialCode: '+352'
  },
  {
      numCode: 'LV',
      nationality: 'لاتفيا',
      dialCode: '+371'
  },
  {
      numCode: 'LY',
      nationality: 'ليبيا',
      dialCode: '+218'
  },
  {
      numCode: 'MA',
      nationality: 'المغرب',
      dialCode: '+212'
  },
  {
      numCode: 'MC',
      nationality: 'موناكو',
      dialCode: '+377'
  },
  {
      numCode: 'MD',
      nationality: 'مولدافيا',
      dialCode: '+373'
  },
  {
      numCode: 'ME',
      nationality: 'الجبل الأسود',
      dialCode: '+382'
  },
  {
      numCode: 'MF',
      nationality: 'سانت مارتين',
      dialCode: '+590'
  },
  {
      numCode: 'MG',
      nationality: 'مدغشقر',
      dialCode: '+261'
  },
  {
      numCode: 'MH',
      nationality: 'جزر المارشال',
      dialCode: '+692'
  },
  {
      numCode: 'MK',
      nationality: 'مقدونيا',
      dialCode: '+389'
  },
  {
      numCode: 'ML',
      nationality: 'مالي',
      dialCode: '+223'
  },
  {
      numCode: 'MM',
      nationality: 'ميانمار',
      dialCode: '+95'
  },
  {
      numCode: 'MN',
      nationality: 'منغوليا',
      dialCode: '+976'
  },
  {
      numCode: 'MO',
      nationality: 'ماكاو الصينية',
      dialCode: '+853'
  },
  {
      numCode: 'MP',
      nationality: 'جزر ماريانا الشمالية',
      dialCode: '+1'
  },
  {
      numCode: 'MQ',
      nationality: 'مارتينيك',
      dialCode: '+596'
  },
  {
      numCode: 'MR',
      nationality: 'موريتانيا',
      dialCode: '+222'
  },
  {
      numCode: 'MS',
      nationality: 'مونتسرات',
      dialCode: '+1'
  },
  {
      numCode: 'MT',
      nationality: 'مالطا',
      dialCode: '+356'
  },
  {
      numCode: 'MU',
      nationality: 'موريشيوس',
      dialCode: '+230'
  },
  {
      numCode: 'MV',
      nationality: 'جزر الملديف',
      dialCode: '+960'
  },
  {
      numCode: 'MW',
      nationality: 'ملاوي',
      dialCode: '+265'
  },
  {
      numCode: 'MX',
      nationality: 'المكسيك',
      dialCode: '+52'
  },
  {
      numCode: 'MY',
      nationality: 'ماليزيا',
      dialCode: '+60'
  },
  {
      numCode: 'MZ',
      nationality: 'موزمبيق',
      dialCode: '+258'
  },
  {
      numCode: 'NA',
      nationality: 'ناميبيا',
      dialCode: '+264'
  },
  {
      numCode: 'NC',
      nationality: 'كاليدونيا الجديدة',
      dialCode: '+687'
  },
  {
      numCode: 'NE',
      nationality: 'النيجر',
      dialCode: '+227'
  },
  {
      numCode: 'NF',
      nationality: 'جزيرة نورفوك',
      dialCode: '+672'
  },
  {
      numCode: 'NG',
      nationality: 'نيجيريا',
      dialCode: '+234'
  },
  {
      numCode: 'NI',
      nationality: 'نيكاراجوا',
      dialCode: '+505'
  },
  {
      numCode: 'NL',
      nationality: 'هولندا',
      dialCode: '+31'
  },
  {
      numCode: 'NO',
      nationality: 'النرويج',
      dialCode: '+47'
  },
  {
      numCode: 'NP',
      nationality: 'نيبال',
      dialCode: '+977'
  },
  {
      numCode: 'NR',
      nationality: 'نورو',
      dialCode: '+674'
  },
  {
      numCode: 'NU',
      nationality: 'نيوي',
      dialCode: '+683'
  },
  {
      numCode: 'NZ',
      nationality: 'نيوزيلاندا',
      dialCode: '+64'
  },
  {
      numCode: 'OM',
      nationality: 'عمان',
      dialCode: '+968'
  },
  {
      numCode: 'PA',
      nationality: 'بنما',
      dialCode: '+507'
  },
  {
      numCode: 'PE',
      nationality: 'بيرو',
      dialCode: '+51'
  },
  {
      numCode: 'PF',
      nationality: 'بولينيزيا الفرنسية',
      dialCode: '+689'
  },
  {
      numCode: 'PG',
      nationality: 'بابوا غينيا الجديدة',
      dialCode: '+675'
  },
  {
      numCode: 'PH',
      nationality: 'الفيلبين',
      dialCode: '+63'
  },
  {
      numCode: 'PK',
      nationality: 'باكستان',
      dialCode: '+92'
  },
  {
      numCode: 'PL',
      nationality: 'بولندا',
      dialCode: '+48'
  },
  {
      numCode: 'PM',
      nationality: 'سانت بيير وميكولون',
      dialCode: '+508'
  },
  {
      numCode: 'PN',
      nationality: 'بتكايرن',
      dialCode: '+872'
  },
  {
      numCode: 'PR',
      nationality: 'بورتوريكو',
      dialCode: '+1'
  },
  {
      numCode: 'PS',
      nationality: 'فلسطين',
      dialCode: '+970'
  },
  {
      numCode: 'PT',
      nationality: 'البرتغال',
      dialCode: '+351'
  },
  {
      numCode: 'PW',
      nationality: 'بالاو',
      dialCode: '+680'
  },
  {
      numCode: 'PY',
      nationality: 'باراجواي',
      dialCode: '+595'
  },
  {
      numCode: 'QA',
      nationality: 'قطر',
      dialCode: '+974'
  },
  {
      numCode: 'RE',
      nationality: 'روينيون',
      dialCode: '+262'
  },
  {
      numCode: 'RO',
      nationality: 'رومانيا',
      dialCode: '+40'
  },
  {
      numCode: 'RS',
      nationality: 'صربيا',
      dialCode: '+381'
  },
  {
      numCode: 'RU',
      nationality: 'روسيا',
      dialCode: '+7'
  },
  {
      numCode: 'RW',
      nationality: 'رواندا',
      dialCode: '+250'
  },
  {
      numCode: 'SA',
      nationality: 'المملكة العربية السعودية',
      dialCode: '+966'
  },
  {
      numCode: 'SB',
      nationality: 'جزر سليمان',
      dialCode: '+677'
  },
  {
      numCode: 'SC',
      nationality: 'سيشل',
      dialCode: '+248'
  },
  {
      numCode: 'SD',
      nationality: 'السودان',
      dialCode: '+249'
  },
  {
      numCode: 'SE',
      nationality: 'السويد',
      dialCode: '+46'
  },
  {
      numCode: 'SG',
      nationality: 'سنغافورة',
      dialCode: '+65'
  },
  {
      numCode: 'SH',
      nationality: 'سانت هيلنا',
      dialCode: '+290'
  },
  {
      numCode: 'SI',
      nationality: 'سلوفينيا',
      dialCode: '+386'
  },
  {
      numCode: 'SJ',
      nationality: 'سفالبارد وجان مايان',
      dialCode: '+47'
  },
  {
      numCode: 'SK',
      nationality: 'سلوفاكيا',
      dialCode: '+421'
  },
  {
      numCode: 'SL',
      nationality: 'سيراليون',
      dialCode: '+232'
  },
  {
      numCode: 'SM',
      nationality: 'سان مارينو',
      dialCode: '+378'
  },
  {
      numCode: 'SN',
      nationality: 'السنغال',
      dialCode: '+221'
  },
  {
      numCode: 'SO',
      nationality: 'الصومال',
      dialCode: '+252'
  },
  {
      numCode: 'SR',
      nationality: 'سورينام',
      dialCode: '+597'
  },
  {
      numCode: 'SS',
      nationality: 'جنوب السودان',
      dialCode: '+211'
  },
  {
      numCode: 'ST',
      nationality: 'ساو تومي وبرينسيبي',
      dialCode: '+239'
  },
  {
      numCode: 'SV',
      nationality: 'السلفادور',
      dialCode: '+503'
  },
  {
      numCode: 'SX',
      nationality: 'سينت مارتن',
      dialCode: '+1'
  },
  {
      numCode: 'SY',
      nationality: 'سوريا',
      dialCode: '+963'
  },
  {
      numCode: 'SZ',
      nationality: 'سوازيلاند',
      dialCode: '+268'
  },
  {
      numCode: 'TC',
      nationality: 'جزر الترك وجايكوس',
      dialCode: '+1'
  },
  {
      numCode: 'TD',
      nationality: 'تشاد',
      dialCode: '+235'
  },
  {
      numCode: 'TF',
      nationality: 'المقاطعات الجنوبية الفرنسية',
      dialCode: '+262'
  },
  {
      numCode: 'TG',
      nationality: 'توجو',
      dialCode: '+228'
  },
  {
      numCode: 'TH',
      nationality: 'تايلند',
      dialCode: '+66'
  },
  {
      numCode: 'TJ',
      nationality: 'طاجكستان',
      dialCode: '+992'
  },
  {
      numCode: 'TK',
      nationality: 'توكيلو',
      dialCode: '+690'
  },
  {
      numCode: 'TL',
      nationality: 'تيمور الشرقية',
      dialCode: '+670'
  },
  {
      numCode: 'TM',
      nationality: 'تركمانستان',
      dialCode: '+993'
  },
  {
      numCode: 'TN',
      nationality: 'تونس',
      dialCode: '+216'
  },
  {
      numCode: 'TO',
      nationality: 'تونجا',
      dialCode: '+676'
  },
  {
      numCode: 'TR',
      nationality: 'تركيا',
      dialCode: '+90'
  },
  {
      numCode: 'TT',
      nationality: 'ترينيداد وتوباغو',
      dialCode: '+1'
  },
  {
      numCode: 'TV',
      nationality: 'توفالو',
      dialCode: '+688'
  },
  {
      numCode: 'TW',
      nationality: 'تايوان',
      dialCode: '+886'
  },
  {
      numCode: 'TZ',
      nationality: 'تانزانيا',
      dialCode: '+255'
  },
  {
      numCode: 'UA',
      nationality: 'أوكرانيا',
      dialCode: '+380'
  },
  {
      numCode: 'UG',
      nationality: 'أوغندا',
      dialCode: '+256'
  },
  {
      numCode: 'UM',
      nationality: 'جزر الولايات المتحدة البعيدة الصغيرة',
      dialCode: ''
  },
  {
      numCode: 'US',
      nationality: 'الولايات المتحدة الأمريكية',
      dialCode: '+1'
  },
  {
      numCode: 'UY',
      nationality: 'أورجواي',
      dialCode: '+598'
  },
  {
      numCode: 'UZ',
      nationality: 'أوزبكستان',
      dialCode: '+998'
  },
  {
      numCode: 'VA',
      nationality: 'الفاتيكان',
      dialCode: '+379'
  },
  {
      numCode: 'VC',
      nationality: 'سانت فنسنت وغرنادين',
      dialCode: '+1'
  },
  {
      numCode: 'VE',
      nationality: 'فنزويلا',
      dialCode: '+58'
  },
  {
      numCode: 'VG',
      nationality: 'جزر فرجين البريطانية',
      dialCode: '+1'
  },
  {
      numCode: 'VI',
      nationality: 'جزر فرجين الأمريكية',
      dialCode: '+1'
  },
  {
      numCode: 'VN',
      nationality: 'فيتنام',
      dialCode: '+84'
  },
  {
      numCode: 'VU',
      nationality: 'فانواتو',
      dialCode: '+678'
  },
  {
      numCode: 'WF',
      nationality: 'جزر والس وفوتونا',
      dialCode: '+681'
  },
  {
      numCode: 'WS',
      nationality: 'ساموا',
      dialCode: '+685'
  },
  {
      numCode: 'XK',
      nationality: 'كوسوفو',
      dialCode: '+383'
  },
  {
      numCode: 'YE',
      nationality: 'اليمن',
      dialCode: '+967'
  },
  {
      numCode: 'YT',
      nationality: 'مايوت',
      dialCode: '+262'
  },
  {
      numCode: 'ZA',
      nationality: 'جمهورية جنوب افريقيا',
      dialCode: '+27'
  },
  {
      numCode: 'ZM',
      nationality: 'زامبيا',
      dialCode: '+260'
  },
  {
      numCode: 'ZW',
      nationality: 'زيمبابوي',
      dialCode: '+263'
  }
];
