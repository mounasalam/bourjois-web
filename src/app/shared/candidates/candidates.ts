import { Token } from '@angular/compiler';

export class Candidates {
  public Id?: number;
  public Token?: string;
  public Email?: string;
  public Gender?: string;
  public Country?: string;
  public FullName?: string;
  public DateOfBirth?: string;
  public Nationality?: string;
  public PhoneNumber?: string;
  public RetailerInvoiceNumber?: string;
}
