import { Component, Input, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  public arLang = false;
  @Input() bgColor = true;

  constructor(
    private translate: TranslateService
  ) {
    this.arLang = (translate.currentLang === 'ar');
   }

  ngOnInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.arLang = (event.lang === 'ar');
    });
  }

}
