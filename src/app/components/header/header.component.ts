import { Component, HostBinding, OnInit, SimpleChanges } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public arLang = false;
  public isNavbarCollapsed = true;
  public starsBackground = false;

  constructor(
    private router: Router,
    private translate: TranslateService) {

    router.events.subscribe((url: any) => {
      if (url.url === '/' || url.url === '/congratulations') {
        this.starsBackground = true;
      } else if (url.url !== undefined) {
        this.starsBackground = false;
      }
    });
    this.arLang = (translate.currentLang === 'ar');
  }

  addClass() {
    const body = document.getElementsByTagName('body')[0];
    if (this.isNavbarCollapsed) {
      body.classList.add('overflow-hidden');
    } else {
      body.classList.remove('overflow-hidden');
    }
  }

  ngOnInit() {
    // document.getElementById('html').style.animationFillMode = 'auto';
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.arLang = (event.lang === 'ar');
    });
  }

  change(lang: string) {
    localStorage.setItem('lan', lang);
    this.translate.use(lang);
  }

}
