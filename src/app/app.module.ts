import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

// IMPORTING COMPONENTS
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CongratulationsComponent } from './pages/congratulations/congratulations.component';
import { DrawDetailsComponent } from './pages/draw-details/draw-details.component';
import { DrawWinnersComponent } from './pages/draw-winners/draw-winners.component';
import { EnterDrawComponent } from './pages/enter-draw/enter-draw.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { Ng2TelInputModule } from 'ng2-tel-input';
// IMPORTING PLUGINS/MODULES
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NumberOnlyDirective } from './directives/numberOnly/number-only.directive';
import { RegistrationComponent } from './pages/registration/registration.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { TermsAndConditionsComponent } from './pages/terms-and-conditions/terms-and-conditions.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    EnterDrawComponent,
    NumberOnlyDirective,
    DrawDetailsComponent,
    RegistrationComponent,
    CongratulationsComponent,
    DrawWinnersComponent,
    TermsAndConditionsComponent,
    PrivacyPolicyComponent,
  ],
  imports: [
    NgbModule,
    FormsModule,
    BrowserModule,
    NgSelectModule,
    HttpClientModule,
    AppRoutingModule,
    Ng2TelInputModule,
    ReactiveFormsModule,
    ScrollToModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
